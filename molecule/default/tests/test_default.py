import testinfra

def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed

def test_nginx_is_running(host):
    nginx = host.service("nginx")
    assert nginx.is_running

def test_nginx_html(host):
    host.run_expect([0], 'curl localhost:8080/test.html | grep HTML')

def test_nginx_php(host):
    host.run_expect([0], 'curl localhost:8080 | grep "PHP Credits"')

